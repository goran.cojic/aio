# examples/server_simple.py
from aiohttp import web

import time
import pymongo
from pandas import DataFrame


async def handle(request):
    num = float(request.match_info.get('num', '0'))
    text = str(num ** 2)
    time.sleep(1)
    print(text)
    return web.Response(text=text)


async def mongo(request):
    # myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    print("jjhfkh")
    myclient = pymongo.MongoClient("mongodb+srv://Coja:userCojaPass@cojaonix.v1wrx.gcp.mongodb.net/BookstoreDb?retryWrites=true&w=majority")
    print(myclient)

    mydb = myclient.get_default_database()
    # mydb = myclient["BookstoreDb"]
    mycol = mydb["Books"]

    text = ''
    for x in mycol.find():
        print(x)
        text = text + str(x) + '\n'

    items_df = DataFrame(mycol.find())
    print(items_df)

    return web.Response(text=text)


async def mongo_insert(request):
    myclient = pymongo.MongoClient("mongodb+srv://Coja:userCojaPass@cojaonix.v1wrx.gcp.mongodb.net/BookstoreDb?retryWrites=true&w=majority")
    mydb = myclient.get_default_database()
    mycol = mydb["Books"]
    data = await request.post()

    mydict = {"Name": data['name'], "Price": data['price'], "Category": data['category'], "Author": data['author']}

    x = str(mycol.insert_one(mydict))
    # print(x)
    return web.Response(text=x)

def mongo_delete(request):
    myclient = pymongo.MongoClient("mongodb+srv://Coja:userCojaPass@cojaonix.v1wrx.gcp.mongodb.net/BookstoreDb?retryWrites=true&w=majority")
    mydb = myclient.get_default_database()
    mycol = mydb["Books"]
    x = str(mycol.delete_many({ "Name": { "$gt": "D" } }))
    return web.Response(text=x)

app = web.Application()
app.add_routes([web.get('/num', handle),
                web.get('/num/{num}', handle),
                web.get('/mongo', mongo),
                web.post('/mongo_insert', mongo_insert),
                web.get('/mongo_delete', mongo_delete)])

if __name__ == '__main__':
    web.run_app(app)
