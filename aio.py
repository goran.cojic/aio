import aiohttp
import asyncio


async def main(num):
    async with aiohttp.ClientSession() as session:
        async with session.get('http://localhost:8080/num/' + num) as response:
            # print("Status:", response.status)
            # print("Content-type:", response.headers['content-type'])
            # html = await response.text()
            # print("Body:", html[:15], "...")

            print(await response.text())


sledeci = True
print("Unosite brojeve. (<empty> - prekid unosa)")
while sledeci:
    num = input()
    if num == '':
        sledeci = False
    else:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main(num))





