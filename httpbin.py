import aiohttp
import asyncio


async def main():
    async with aiohttp.ClientSession('http://httpbin.org') as session:
        d = {'key1': 'value1', 'key2': ['value2', 'value3']}
        # d = 'jksakhd a'
        async with session.post('/post', data=d) as resp:
            print(await resp.text())
            print(resp.url)

        async with session.get('/get') as resp:
            print(resp.status)
            print(await resp.text())

        async with session.put('/put', data=b'data'):
            pass

        params = {'key1': 'value1', 'key2': ['value2', 'value3']}
        async with session.get('/get', params=params) as resp:
            print(await resp.text())
            print(resp.url)

        await session.close()


asyncio.run(main())